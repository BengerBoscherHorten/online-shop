package de.fhswf.fit;

import java.io.Serializable;
import java.util.List;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceUnit;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;

/**
 * Service-Klasse für die Kategorie
 **/ 
@PersistenceUnit
@Stateless
public class CategoryServices implements Serializable{
    @PersistenceContext
    EntityManager em;
    
    public List<Category> getAll() {
        System.out.println("GetAll");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Category> cq = cb.createQuery(Category.class);
        Root<Category> root = cq.from(Category.class);
        cq.select(root);
        TypedQuery<Category> query = em.createQuery(cq);
        return query.getResultList();
    }
    /**
     * Gibt eine Kategorie zu einer ID
     * @param b Kategorie die hinzugefügt werden soll
     */
    public Category getById(int id) {
        Category b = em.find(Category.class, id);
        return b;
    }
    /**
     * Fügt  eine Kategorie hinzu
     * @param b Kategorie die hinzugefügt werden soll
     */
    public Category add(Category b) {
        System.out.println("Add");
        em.persist(b);
        return b;
    }
    /**
     * Speichert eine Kategorie
     * @param b Kategorie die gespeichert werden soll
     */
    public Category update(Category b) {
        System.out.println("Update");
        em.merge(b);
        return b;
    }
    /**
     * Löscht eine Kategorie
     * @param b Kategorie die gelöscht werden soll
     */
    public void delete(Category b) {
        b = em.merge(b);
        em.remove(b);
    }
}


