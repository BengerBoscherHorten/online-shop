package de.fhswf.fit;

import java.io.Serializable;
import java.util.List;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceUnit;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;

/**
 * Sevice-Klasse für die Datenbanktabelle Product.
 */
@PersistenceUnit
@Stateless 
public class ProductServices implements Serializable {
    
    @PersistenceContext
    EntityManager em;

    /**
     * Gibt alle gespeicherten Products zurück.
     * 
     * @return Product
     */
    public List<Product> getAll() {
        System.out.println("GetAll");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Product> cq = cb.createQuery(Product.class);
        Root<Product> root = cq.from(Product.class);
        cq.select(root);
        TypedQuery<Product> query = em.createQuery(cq);
        return query.getResultList();
    } 
    
    /**
     * Liefert das Product zu einer id.
     * 
     * @param id entspricht der productNumber von Product.
     * @return Product
     */
    public Product getById(String id) {
        Product b = em.find(Product.class, id);
        return b;
    }

    /**
     * Fügt ein neues Product hinzu.
     * 
     * @param p Product welches hinzugefügt wird.
     * @return Product
     */
    public Product add(Product p) {
        em.persist(p);
        return p;
    }

    /**
     * Aktualisiert ein bereits existierendes Product.
     * 
     * @param p Product welches aktualisiert wird.
     * @return Product
     */
    public Product update(Product p) {
        em.merge(p);
        return p;
    }

    /**
     * Löscht ein Product.
     * 
     * @param p Product welches gelöscht wird.
     */
    public void delete(Product p) {
        if(!em.contains(p))
            p = em.merge(p);
        em.remove(p);
    }
}
