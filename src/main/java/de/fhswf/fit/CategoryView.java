package de.fhswf.fit;

import java.util.List;

import org.primefaces.event.RowEditEvent;

import java.io.Serializable;
import java.util.ArrayList;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import jakarta.inject.Inject;

@Named("categoryView")
@SessionScoped
public class CategoryView implements Serializable {
    
    transient CategoryServices categoryServices;

    private List<Category> kategorien = new ArrayList<Category>();


    public CategoryView(){
    }
    
    public void init() {
        System.out.println("initializing CategoryView");

        List<Category> kategorien = this.categoryServices.getAll();
        this.kategorien.addAll(kategorien);
    }

    @Inject
    public void setCategoryServices(CategoryServices categoryServices) {
        this.categoryServices = categoryServices;
        init();
    }

    public List<Category> getKategorien() {
        return kategorien;
    }

    public void onAddNew() {
        Category newCategory = new Category("NeueKategorie", "KategorieBeschreibung");
        newCategory = categoryServices.add(newCategory);
        kategorien.add(newCategory);
    }

    public void onRowEdit(RowEditEvent<Category> event) {
        try {
            Category category = event.getObject();
            System.out.println("rowEdit: " + category);
            categoryServices.update(category);
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
        }     
    }

    
}
