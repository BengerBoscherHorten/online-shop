package de.fhswf.fit;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;

@Named("productView")
@SessionScoped 
public class ProductView implements Serializable {
    
    transient ProductServices productServices;

    private List<Product> products = new ArrayList<Product>();
    private Product selectedProduct;

    public ProductView(){
    }

    public void init() {
        System.out.println("inistializing Products");

        List<Product> products = this.productServices.getAll();
        this.products.addAll(products);
    }

    @Inject
    public void setProductServices(ProductServices productServices) {
        this.productServices = productServices;
        init();
    }

    public List<Product> getProducts() {
        return products;
    }

    public void deleteProduct(String id){
        Product p = productServices.getById(id);
        productServices.delete(p);
        products = productServices.getAll();
    }

    public void saveProduct(String productNumber, String name, BigDecimal price, Integer inStock, String description){
        Product product = new Product(productNumber, name, price, inStock, description);
        saveProduct(product);
    }

    public void saveProduct(Product product){
        productServices.update(product);
        products = productServices.getAll();
    }

    public Product getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(Product selectedProduct) {
        this.selectedProduct = selectedProduct;
    }
}
