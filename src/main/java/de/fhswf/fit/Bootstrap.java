package de.fhswf.fit;

import java.io.File;
import java.math.BigDecimal;
import java.net.URI;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jakarta.annotation.PostConstruct;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.inject.Inject;

@Startup
@Singleton
public class Bootstrap {
    @Inject
    CategoryServices categoryServices;
    @Inject
    ImageServices imageServices;
    @Inject
    ProductServices productServices;

    @PostConstruct
    public void init() {
        System.out.println("bootstraping application...");

        List<Category> kategorien = categoryServices.getAll();
        if(kategorien.isEmpty()) {
            kategorien.add(categoryServices.add(new Category("Spielwaren", "Produkte die hauptsächlich an Kinder gerichtet sind")));
            kategorien.add(categoryServices.add(new Category("Elektronik", "Produkte die mit Strom arbeiten")));
            kategorien.add(categoryServices.add(new Category("Lebensmittel", "Speisen oder Getränke für denn Verzehr")));
            kategorien.add(categoryServices.add(new Category("Bürobedarf", "Produkte die im Büros eingesetzt werden")));
            System.out.println(kategorien.size() + "Categorys added");
        }
        
        List<Image> bilder = imageServices.getAll();
        if(bilder.isEmpty()) {
            try {
                URI path = new URI(Objects.requireNonNull(this.getClass().getClassLoader().getResource("images/").getPath()));
                System.out.println(path);
                File imageFile1 = new File(path + "/usbstick.jpg");
                byte[] bilddaten1 = Files.readAllBytes(imageFile1.toPath());
                bilder.add(imageServices.add(new Image("USB-Stick", bilddaten1, ImageType.JPEG)));

                File imageFile2 = new File(path + "/kugelschreiber.png");
                byte[] bilddaten2 = Files.readAllBytes(imageFile2.toPath());
                bilder.add(imageServices.add(new Image("Kugelschreiber", bilddaten2, ImageType.PNG)));

                File imageFile3 = new File(path + "/fussball.jpg");
                byte[] bilddaten3 = Files.readAllBytes(imageFile3.toPath());
                bilder.add(imageServices.add(new Image("Fussball", bilddaten3, ImageType.JPEG)));

                File imageFile4 = new File(path + "/pizza.jpg");
                byte[] bilddaten4 = Files.readAllBytes(imageFile4.toPath());
                bilder.add(imageServices.add(new Image("Pizza", bilddaten4, ImageType.JPEG)));

                File imageFile5 = new File(path + "/pizza2.jpg");
                byte[] bilddaten5 = Files.readAllBytes(imageFile5.toPath());
                bilder.add(imageServices.add(new Image("Pizza alternativ", bilddaten5, ImageType.JPEG)));

                System.out.println(bilder.size() + "Images added");
            } catch (Exception e) {
                System.out.println("Fehler:" + e.getMessage());
                e.printStackTrace();
            }
        }

        List<Product> produkte = productServices.getAll();
        if(produkte.isEmpty()) {
            List<Category> categorys1 = new ArrayList<Category>();
            categorys1.add(kategorien.get(1));
            List<Image> images1 = new ArrayList<Image>();
            images1.add(bilder.get(0));
            produkte.add(productServices.add(new Product("P2022-00001","USB-Stick", new BigDecimal("45.95"), 400, "Ein Gerät zum Speichern von Daten", categorys1, images1)));

            List<Category> categorys2 = new ArrayList<Category>();
            categorys2.add(kategorien.get(3));
            List<Image> images2 = new ArrayList<Image>();
            images2.add(bilder.get(1));
            produkte.add(productServices.add(new Product("P2022-00002","Kugelschreiber", new BigDecimal("2.95"), 2000, "Ein Gerät zum Schreiben auf Papier", categorys2, images2)));

            List<Category> categorys3 = new ArrayList<Category>();
            categorys3.add(kategorien.get(0));
            List<Image> images3 = new ArrayList<Image>();
            images3.add(bilder.get(2));
            produkte.add(productServices.add(new Product("P2022-00003","Fussball", new BigDecimal("22.49"), 40, "Ein Gerät zum Fussballspielen", categorys3, images3)));

            List<Category> categorys4 = new ArrayList<Category>();
            categorys4.add(kategorien.get(2));
            List<Image> images4 = new ArrayList<Image>();
            images4.add(bilder.get(3));
            images4.add(bilder.get(4));
            produkte.add(productServices.add(new Product("P2022-00004","Pizza", new BigDecimal("6.50"), 70, "Eine leckere Pizza", categorys4, images4)));

            System.out.println(produkte.size() + "Products added");
        }  
    }  

}
