package de.fhswf.fit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToMany;

@Entity
public class Product {

    @Id
    private String productNumber;

    @Column
    private String name;

    @Column
    private BigDecimal price;
    
    @Column
    private Integer inStock;

    @Column
    private String description;

    @ManyToMany(mappedBy = "products")
    List<Category> categories;

    @ManyToMany 
    List<Image> images;

    public Product(){
    } 

    public Product(String productNumber, String name, BigDecimal price, Integer inStock, String description) {
        this.productNumber = productNumber;
        this.name = name;
        this.price = price;
        this.inStock = inStock;
        this.description = description;
        this.categories = new ArrayList<Category>();
    }
    public Product(String productNumber, String name, BigDecimal price, int inStock, String description,
        List<Category> categories, List<Image> images) {
        this.productNumber = productNumber;
        this.name = name;
        this.price = price;
        this.inStock = inStock;
        this.description = description;
        this.categories = categories;
        this.images = images;
    }

    public String getProductNumber() {
        return productNumber;
    }
    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public BigDecimal getPrice() {
        return price;
    }
    public void setPrice(BigDecimal price) {
        this.price = price;
    }
    public Integer getInStock() {
        return inStock;
    }
    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public List<Category> getCategories() {
        return categories;
    }
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
    public List<Image> getImages() {
        return images;
    }
    public void setImages(List<Image> images) {
        this.images = images;
    } 

    public void addCategory(Category category) {
        categories.add(category);
    } 

    public void addImage(Image image) {
        images.add(image);
    }

    public String getStatus(){
        if(inStock == 0)
            return "AUSVERKAUFT";
        if(inStock < 4)
            return "WENIG AUF LAGER";
        return "AUF LAGER";
    } 
}