package de.fhswf.fit;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import jakarta.persistence.ManyToMany;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Image {
    @Id
    @GeneratedValue
    private int id;

    @Column
    private String name;

    @Lob
    private byte[] data;

    @Enumerated(EnumType.STRING)
    private ImageType type;

    @ManyToMany (mappedBy = "images")
    List<Product> products;

    public Image(){
    } 

    public Image(String name, byte[] data, ImageType type) {
        this.name = name;
        this.data = data;
        this.type = type;
        products = new ArrayList<Product>();
    }

    public Image(int id, String name, byte[] data, ImageType type, List<Product> products) {
        this.id = id;
        this.name = name;
        this.data = data;
        this.type = type;
        this.products = products;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public ImageType getType() {
        return type;
    }

    public void setType(ImageType type) {
        this.type = type;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

   
}
