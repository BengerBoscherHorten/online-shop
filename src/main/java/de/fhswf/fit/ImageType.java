package de.fhswf.fit;

public enum ImageType {
    JPEG,
    PNG,
    GIF;
}
