package de.fhswf.fit;

import java.io.Serializable;
import java.util.List;

import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.PersistenceUnit;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;

/**
 * Sevice-Klasse für die Datenbanktabelle Image.
 */
@PersistenceUnit
@Stateless 
public class ImageServices implements Serializable {
    
    @PersistenceContext
    EntityManager em;

    /**
     * Gibt alle gespeicherten Images zurück.
     * 
     * @return Image
     */
    public List<Image> getAll() {
        System.out.println("GetAll");
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Image> cq = cb.createQuery(Image.class);
        Root<Image> root = cq.from(Image.class);
        cq.select(root);
        TypedQuery<Image> query = em.createQuery(cq);
        return query.getResultList();
    } 
    
    /**
     * Liefert das Image zu einer id.
     * 
     * @param id entspricht der id von Image.
     * @return Image
     */
    public Image getById(int id) {
        Image b = em.find(Image.class, id);
        return b;
    }

    /**
     * Fügt ein neues Image hinzu.
     * 
     * @param p Image welches hinzugefügt wird.
     * @return Image
     */
    public Image add(Image p) {
        em.persist(p);
        return p;
    }

    /**
     * Aktualisiert ein bereits existierendes Image.
     * 
     * @param p Image welches aktualisiert wird.
     * @return Image
     */
    public Image update(Image p) {
        em.merge(p);
        return p;
    }

    /**
     * Löscht ein Image.
     * 
     * @param p Image welches gelöscht wird.
     */
    public void delete(Image p) {
        if(!em.contains(p))
            p = em.merge(p);
        em.remove(p);
    }
}
